function ble2can_obj = ble2can_connect()
    %scan for BLE peripherals
    blelist("Timeout",4)

    %creat ble object for Axiomatic CAN to BLE convertor
    ble2can_obj = ble("CAN-BT-APPL")
end
