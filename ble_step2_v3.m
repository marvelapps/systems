
function ble_step2_v3(src,event)
%clear all

src

%scan for BLE peripherals
blelist("Timeout",4)

%creat ble object for Axiomatic CAN to BLE convertor
CAN2BLE = ble("CAN-BT-APPL")

%connecting to a Write service 571
canbleTxCharacteristic = characteristic(CAN2BLE,...
                            "2B68C56E-8E48-11E7-BB31-BE2E44B06B34",...
                            "2B68C571-8E48-11E7-BB31-BE2E44B06B34");

%declare a message data structure object
CANPrefabMsg_01 = [19,15,2,0,255,24,8,17,255,52,34,86,238,171,9,202,53,102,94]
% Send Ext.ID CAN Frame (ID=0x18FF0002, len=8, data: 0x11 0xff 0x34 0x22 0x56 0xee 0xab 0x09):...
% 0x13 0x0f 0x02 0x00 0xff 0x18 0x08 0x11 0xff 0x34 0x22 0x56 0xee 0xab 0x09 0xca 0x35 0x66 0x5e 

CANPrefabMsg_02 = [0x12,0x0d,0xf0,0x01,0x08,0x11,0xff,0x34,0x22,0x56,0xee,0xab,0x09,0xbf,0xc8,0xa1,0xe7]
% Send Std.ID CAN Frame (ID=0x1F0, len=8, data: 0x11 0xff 0x34 0x22 0x56 0xee 0xab 0x09):...
%  0x12 0x0d 0xf0 0x01 0x08 0x11 0xff 0x34 0x22 0x56 0xee 0xab 0x09 0xbf 0xc8 0xa1 0xe7  

%Writing to the message to the characteristic of connected Write Service 
write(canbleTxCharacteristic,CANPrefabMsg_01)
write(canbleTxCharacteristic,CANPrefabMsg_02)

%message received by Kvaser from CAN2BT in response to ble charactertistic write:
% 0    18FF0002 X       8  11  FF  34  22  56  EE  AB  09    3144.916950 R
% 0    000001F0         8  11  FF  34  22  56  EE  AB  09   69078.592950 R

% Power Up Message from CAN2BT
% 0    18EEFF80 X       8  01  87  50  14  90  19  00  80   56012.597680 R



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read message from BLE device CAN2BT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%connecting to a Custom Notify service and reading it's characteristic data
% these UUIDs are from the blelist results
canbleRXcharacteristic = characteristic(CAN2BLE,...
                        "2B68C56E-8E48-11E7-BB31-BE2E44B06B34",...
                        "2B68C570-8E48-11E7-BB31-BE2E44B06B34")
%subscribe to service               
subscribe(canbleRXcharacteristic)

%Assign a method to the DataAvailable callback function of this service
canbleRXcharacteristic.DataAvailableFcn = @displayCharacteristicData

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Need different method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%callback funtion for characteristic Data Available
function displayCharacteristicData(src,~)
    % src is handle to data from the subscribed service
    [CANRX,timestamp] = read(src,'oldest');
    CANRXMsgType = sprintf("%d",CANRX(1));
    CANRXMsgLen = sprintf("%d",CANRX(2));
    CANRXMsgId = sprintf("%x",CANRX(6),CANRX(5),CANRX(4),CANRX(3));
    sprintf("%s ",CANRXMsgType,CANRXMsgLen,CANRXMsgId);
    disp(timestamp);
    disp(CANRXMsgId);
end

 
%deconstruct CAN message (spec from Axiomatic)

% for x=1:10 % read ten messages in a row
% [CANRX,timestamp] = read(canbleRXcharacteristic,'oldest');
% CANRXMsgType = sprintf("%d",CANRX(1));
% CANRXMsgLen = sprintf("%d",CANRX(2));
% CANRXMsgId = sprintf("%x",CANRX(6),CANRX(5),CANRX(4),CANRX(3));
% sprintf("%s ",CANRXMsgType,CANRXMsgLen,CANRXMsgId)
% end % end read ten messages in a row

% CANRXMsgDLC = CANRX(7)
% CANRXMsgPayLoad = [ CANRX(8) CANRX(9) CANRX(10) CANRX(11) CANRX(12) CANRX(13) CANRX(14) CANRX(15)]
% CANRXMsgChksum01 = CANRX(16)
% CANRXMsgChksum02 = CANRX(17)
% CANRXMsgChksum03 = CANRX(18)
% CANRXMsgChksum04 = CANRX(19)
% 
% CANRXMsgChksum = [ CANRX( [ 19 18 17 16] ) ] % reverse byte order for checksum verification
% 
% CANRXMsgIdHex = dec2hex( 10.0^0*CANRX(3) + 10.0^1*CANRX(4) + 10.0^3*CANRX(5) + 10.0^4*CANRX(6) )
% 
% 
% %Verify checksum
% chksum = crc32( CANRXMsgChksum )  %calculate 32 bit checksum of the RX message payload
% CANRXChksumRec = 10.0^0*CANRX(16) + 10.0^1*CANRX(17) + 10.0^3*CANRX(18) + 10.0^4*CANRX(19)







% 
%connect to the 52 module
%bluefruit = ble("Bluefruit52")
% 
% %connect to a custom Notify service and read its characteristic data
% d = characteristic(bluefruit,["6E400001-B5A3-F393-E0A9-E50E24DCCA9E"],["6E400003-B5A3-F393-E0A9-E50E24DCCA9E"])
% BTRX = read(d)
% 
% %connect to custom write service and send it some data
% c = characteristic(bluefruit, ["00001530-1212-EFDE-1523-785FEABCD123"], ["00001532-1212-EFDE-1523-785FEABCD123"])
% write(c,[0 1 2 3],'withoutresponse')
% 
% %Connect to misc service and read its characteristic data
% p = characteristic(bluefruit,"Generic Access","Peripheral Preferred Connection Parameters")
% fruitpref= read(p)


% chans = canChannelList


%  can message dump from Kvaser:
%CAN2BT power-up message:
% 0    18EEFF80 X       8  01  87  50  14  90  19  00  80    2800.558720 R

%Mikes test messages:
% 0    18FAFD81 X       8  0F  0E  0D  0C  0C  0B  0A  08    2963.344680 R
% 0    18FAFE80 X       8  01  02  03  04  05  06  07  08    2963.844480 R


% %Create Object for SparkFun Artemis BLE 
% ArtBLE = ble("Artemis BLE")
% 
% %connecting to a Write service 
% ArtBLETxCharacteristic = characteristic(ArtBLE,"Immediate Alert","Alert Level")
% 
% % Messaage to Turn LED on
% ArtPrefabMsg_01 = 0x11
% ArtPrefabMsg_02 = 0x00
% 
% %Writing to the message to the characteristic of connected Write Service 
% write(ArtBLETxCharacteristic,ArtPrefabMsg_01)
end
