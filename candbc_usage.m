%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Vehicle Network Toolbox CAN stuff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Open the CAN database file (DBC file)
db = canDatabase('BleLink_Msgs_v01.dbc');

%define the CAN channel to be opened
canch = canChannel("Kvaser","Leaf Light v2 1",1);% returns a CAN channel connected to a device from a specified vendor.

%open CAN channel for writing (choose to use seperate channels (same
%physical channel
start(canch);

% Create an object of type canMessage, referencing a message name
msg1 = canMessage(db,'CAN_DCM_BleLink_State');
msg2 = canMessage(db,'BleLink_Tmp');

% assign values to signal variables
msg1.Signals.state_cur = 99;
msg2.Signals.Tmp_byte_0 = 232;
  
%transmit the messages on the open channel
transmit(canch,[msg1,msg2]);

%stop channel and clear the object (memory construct)
stop(canch);
clear canch;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create and open a channel to for Reading...
% (same physical channel as write)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rxCh = canChannel("Kvaser","Leaf Light v2 1",1);
start(rxCh);

% Attach database to channel
rxCh.Database = db;

%generateMsgsDb();
%rxMsg = receive(rxCh, Inf, 'OutputFormat', 'timetable');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create Timer Object:  Wait for messages to arrive
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T = timer('TimerFcn',@(~,~)disp('Ten Second Delay Complete'),'StartDelay',10);
start(T)  %  Start timer
wait(T)   %  Block command line until timer expires
delete(T) %  Delete timer object

%Read 15 messages
rxMsg = receive(rxCh, 15, 'OutputFormat', 'timetable');

% create an object of type Timetable from message 'RxFromPeriphPeriodic
signalTimetable = canSignalTimetable(rxMsg, 'RxFromPeriphPeriodic');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot a signal byte 0 from message RxFromPeriphPeriod
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plot(signalTimetable.Time, signalTimetable.SigByte_0)
title('SigByte_0 from RxFromPeriphPeriodic', 'FontWeight', 'bold')
xlabel('Timestamp')
ylabel('SigByte_0')

%stop channel and clear the object
stop(rxCh);
clear rxCh

