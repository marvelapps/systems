function [CANRX,timestamp] = ble2can_receive(ble2can_obj)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read message from BLE device CAN2BT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% connecting to a Custom Notify service and reading it's characteristic data
% these UUIDs are from the blelist results
canbleRXcharacteristic = characteristic(ble2can_obj,...
                        "2B68C56E-8E48-11E7-BB31-BE2E44B06B34",...
                        "2B68C570-8E48-11E7-BB31-BE2E44B06B34");
                    
[CANRX,timestamp] = read(canbleRXcharacteristic,'oldest');

% CANRXMsgType = sprintf("%d",CANRX(1));
% CANRXMsgLen = sprintf("%d",CANRX(2));
% CANRXMsgId = sprintf("%x",CANRX(6),CANRX(5),CANRX(4),CANRX(3));
% disp(timestamp);
% disp(CANRXMsgId);

    
%subscribe to service               
% %
% % Assign a method to the DataAvailable callback function of this service
% canbleRXcharacteristic.DataAvailableFcn = @displayCharacteristicData
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%  Need different method
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % callback funtion for characteristic Data Available
% function displayCharacteristicData(src,~)
%     % src is handle to data from the subscribed service
%     [CANRX,timestamp] = read(src,'oldest');
% %     CANRXMsgType = sprintf("%d",CANRX(1));
% %     CANRXMsgLen = sprintf("%d",CANRX(2));
% %     CANRXMsgId = sprintf("%x",CANRX(6),CANRX(5),CANRX(4),CANRX(3));
%     disp(CANRXMsgType,CANRXMsgLen,CANRXMsgId);
%     disp(timestamp);
%     disp(CANRXMsgId);
%
end