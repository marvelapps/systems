function ble2can_send(ble2can_obj,src,event)

%connecting to a Write service 571
canbleTxCharacteristic = characteristic(ble2can_obj,...
                            "2B68C56E-8E48-11E7-BB31-BE2E44B06B34",...
                            "2B68C571-8E48-11E7-BB31-BE2E44B06B34");

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Construct BLE2CAN Message (all array elements are decimal base)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MessageType = hex2dec('31'); % See Axiomatic spec
MessageLen = 15;  % see axiomatic spec
CANMsgID = uint32( hex2dec('18FAFD77')); % hex value of extended ID

% convert to little endian
CANMsgIDle = dec2hex(swapbytes(CANMsgID));   
CANMsgIDle01 = hex2dec( CANMsgIDle([1 2]) );
CANMsgIDle02 = hex2dec( CANMsgIDle([3 4]) );
CANMsgIDle03 = hex2dec( CANMsgIDle([5 6]) );
CANMsgIDle04 = hex2dec( CANMsgIDle([7 8]) );

CANDLC = 8;   %number of message bytes
CANData = [src(1) src(2) src(3) src(4) 251 250 249 248];  %these are CAN message contents

% Build packet that will become the CAN message
CANTXPacket = [ MessageType MessageLen...
                     CANMsgIDle01 CANMsgIDle02 CANMsgIDle03 CANMsgIDle04...
                          CANDLC ...
                              CANData ];
        
%calculate 32 bit checksum of the BLE message payload  
chksum = dec2hex( crc32(CANTXPacket) );   

%annex the checksum in reverse byte order (little endian) to the end of the
%BLE message payload
CANTX = [CANTXPacket hex2dec( chksum([7 8])) hex2dec(chksum([5 6]))...
                           hex2dec(chksum([3 4])) hex2dec(chksum([1 2])) ];
                         
%Writing the message to the characteristic of connected Write Service 
write(canbleTxCharacteristic,CANTX);

end