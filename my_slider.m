%Create an object of class (type) figure call it hfig (handle)
hfig = figure();

%Within the figure class create a slider object then define properties via
%"property-value pairs"
slider = uicontrol('Parent', hfig,'Style','slider',...
         'Units','normalized',...
         'Position',[0.3 0.5 0.4 0.1],...% [left bottom width height]
         'Tag','slider1',...
         'UserData',struct('val',0,'diffMax',1),...
         'Callback',@slider_callback);
     
%Within the figure class create a button object, the define properties via
%"property-value pairs"
button = uicontrol('Parent', hfig,'Style','pushbutton',...
         'Units','normalized',...
         'Position',[0.5 0.3 0.2 0.1],...%'Position',[0.4 0.3 0.2 0.1],...
         'String','Send msg to CAN2BT',...
         'Callback',@button_callback);
     
%display workspace and size
whos

function slider_callback(hObject,eventdata)
    sval = hObject.Value;
	diffMax = hObject.Max - sval;
	data = struct('val',sval,'diffMax',diffMax);
	hObject.UserData = data;
end

function button_callback(hObject,eventdata)
    disp("connecting to CAN2BT...");
	h = findobj('Tag','slider1');
	data = h.UserData;
	disp([data.val data.diffMax]);
    candbc_usage;
end