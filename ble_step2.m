clear all

%scan for BLE peripherals
blelist("Timeout",20)

%creat ble object
CAN2BLE = ble("CAN-BT-APPL")

%
%Construct Can Message (all array elements are decimal base)
%
MessageType = hex2dec('31')
MessageLen = 15                             %
CANMsgID = uint32( hex2dec('18FAFD79'))     %hex value of extended ID
% convert to little endian
CANMsgIDle = dec2hex(swapbytes(CANMsgID))   
CANMsgIDle01 = hex2dec( CANMsgIDle([1 2]) )
CANMsgIDle02 = hex2dec( CANMsgIDle([3 4]) )
CANMsgIDle03 = hex2dec( CANMsgIDle([5 6]) )
CANMsgIDle04 = hex2dec( CANMsgIDle([7 8]) )
CANDLC = 8                                   %number of message bytes
CANData = [255 254 253 252 251 250 249 248]  %these are CAN message contents

CANTXPacket = [ MessageType MessageLen CANMsgIDle01 CANMsgIDle02...
            CANMsgIDle03 CANMsgIDle04 CANDLC CANData ]
        
chksum = dec2hex( crc32(CANTXPacket) )       %calculate 32 bit checksum of the BLE message payload
%annex the checksum in reverse byte order (little endian) to the end of the
%BLE message payload

CANTX = [CANTXPacket hex2dec( chksum([7 8])) hex2dec(chksum([5 6])) hex2dec(chksum([3 4])) hex2dec(chksum([1 2])) ]

%Write CAN message
%connecting to a Write service and writing characteristic data
canbleTxCharacteristic = characteristic(CAN2BLE,["2B68C56E-8E48-11E7-BB31-BE2E44B06B34"],["2B68C571-8E48-11E7-BB31-BE2E44B06B34"])
write(canbleTxCharacteristic,CANTX)

%Read CAN message
%connecting to a Custom Notify service and reading it's characteristic data
canbleReccharicteristic = characteristic(CAN2BLE,"2B68C56E-8E48-11E7-BB31-BE2E44B06B34","2B68C570-8E48-11E7-BB31-BE2E44B06B34")

CANRX = read(canbleReccharicteristic)

%deconstruct CAN message (spec from Axiomatic)
CANRXMsgType = CANRX(1)
CANRXMsgLen = CANRX(2)
CANRXMessageIDLe01 = CANRX(3)
CANRXMessageIDLe02 = CANRX(4)
CANRXMessageIDLe03 = CANRX(5)
CANRXMessageIDLe04 = CANRX(6)
CANRXMsgDLC = CANRX(7)
CANRXMsgPayLoad = [ CANRX(8) CANRX(9) CANRX(10) CANRX(11) CANRX(12) CANRX(13) CANRX(14) CANRX(15)]
CANRXMsgChksum01 = CANRX(16)
CANRXMsgChksum02 = CANRX(17)
CANRXMsgChksum03 = CANRX(18)
CANRXMsgChksum04 = CANRX(19)

CANRXMsgChksum = [ CANRX( [ 19 18 17 16] ) ] % reversere byte order for checksum verification

% 
% %connect to the 52 module
% bluefruit = ble("Bluefruit52")
% 
% %connect to a custom Notify service and read its characteristic data
% d = characteristic(bluefruit,["6E400001-B5A3-F393-E0A9-E50E24DCCA9E"],["6E400003-B5A3-F393-E0A9-E50E24DCCA9E"])
% BTRX = read(d)
% 
% %connect to custom write service and send it some data
% c = characteristic(bluefruit, ["00001530-1212-EFDE-1523-785FEABCD123"], ["00001532-1212-EFDE-1523-785FEABCD123"])
% write(c,[0 1 2 3],'withoutresponse')
% 
% %Connect to misc service and read its characteristic data
% p = characteristic(bluefruit,"Generic Access","Peripheral Preferred Connection Parameters")
% fruitpref= read(p)
